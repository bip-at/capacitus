// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const {
    ActivityHandler,
    MessageFactory,
    CardFactory,
    TurnContext,
    teamsGetChannelId
} = require('botbuilder');

const { ActionTypes } = require('botframework-schema');

const actions = ['Abfrage Kapazität', 'Steuerung AGLA'];


class MessageReactionBot extends ActivityHandler {
    constructor(activityLog, garageShortNames) {
        super();
        this._log = activityLog;
        // Dependency injected dictionary for storing ConversationReference objects used in NotifyController to proactively message users
//this.conversationReferences = conversationReferences;
        this.garageShortNames = garageShortNames
        this.onMembersAdded(async (context, next) => {
            await this.sendWelcomeMessage(context);

            // By calling next() you ensure that the next BotHandler is run.
            await next();
        });

        this.onConversationUpdate(async (context, next) => {
          //  this.addConversationReference(context.activity);

            await next();
        });

        this.onMembersAdded(async (context, next) => {
            const membersAdded = context.activity.membersAdded;
            for (let cnt = 0; cnt < membersAdded.length; cnt++) {
                if (membersAdded[cnt].id !== context.activity.recipient.id) {
                    const welcomeMessage = 'Welcome to the Proactive Bot sample.  Navigate to http://localhost:3978/api/notify to proactively message everyone who has previously messaged this bot.';
                    await context.sendActivity(welcomeMessage);
                }
            }

            // By calling next() you ensure that the next BotHandler is run.
            await next();
        });


        this.onEvent(async (context, next) => {
            console.log("onEvent");
        })

        this.onMessage(async (context, next) => {
            //await this.sendMessageAndLogActivityId(context, `echo: ${ context.activity.text }`);
            //await next();
            console.log("onMessage");


            const text = context.activity.text;

            // Create an array with the valid color options.

            

            // If the `text` is in the Array, a valid color was selected and send agreement.
            if (text == actions[0]) {
                await this.sendMessageAndLogActivityId(context, `Going to fetch numbers of garage`);
            } else if (text == actions[1]) {
                await this.sendMessageAndLogActivityId(context, `AGLA Beschilderung wird aufgerufen`);
            } else {
                console.log("text: "+text);
                console.log("text short: "+text.split('</at>'));
                //const number = text.match(/(\d+)/);
                //console.log("number: "+number);
                //if (number != null) {
                    const input = text.split('</at>')[1].trim();
                    const inputSplit = input.split(":");

                    const garageShortName = input.split(":")[0];
                    const threshold = input.split(":")[1] ?? 5;

                    this.addConversationReference(context.activity, garageShortName, threshold);
                //}
                //await this.sendMessageAndLogActivityId(context, `Please select a color`);    
            }

            

            // After the bot has responded send the suggested actions.
            //await this.sendSuggestedActions(context);

            /*
            const teamsChannelId = teamsGetChannelId(context.activity);
            console.log("teams channel ID:" +teamsChannelId);
            console.log("serviceurl:" +context.activity.serviceUrl);
            const message = MessageFactory.text('This will be the first message in a new thread');
            const newConversation = await this.teamsCreateConversation(context, teamsChannelId, message);

            await context.adapter.continueConversation(newConversation[0],
                async (t) => {
                    
                    await t.sendActivity(MessageFactory.text('This will be the first response to the new thread'));
                }
            );
            */

            // By calling next() you ensure that the next BotHandler is run.
            await next();

        });
    }

    addConversationReference(activity, garageShortName, threshold) {
        
        const conversationReference = TurnContext.getConversationReference(activity);
        conversationReference.threshold = threshold;
        conversationReference.garageShortName = garageShortName;
        //this.conversationReferences[conversationReference.conversation.id] = conversationReference;
        this.garageShortNames[garageShortName] = conversationReference;
        console.log("addings conversation ref: "+JSON.stringify(this.garageShortNames));
    }

    async teamsCreateConversation(context, teamsChannelId, message) {
        const conversationParameters = {
            isGroup: true,
            channelData: {
                channel: {
                    id: teamsChannelId
                }
            },

            activity: message
        };

        const connectorClient = context.adapter.createConnectorClient(context.activity.serviceUrl);
        const conversationResourceResponse = await connectorClient.conversations.createConversation(conversationParameters);
        const conversationReference = TurnContext.getConversationReference(context.activity);
        conversationReference.conversation.id = conversationResourceResponse.id;
        return [conversationReference, conversationResourceResponse.activityId];
    }

    async onReactionsAddedActivity(reactionsAdded, context) {
        for (var i = 0, len = reactionsAdded.length; i < len; i++) {
            var activity = await this._log.find(context.activity.replyToId);
            if (!activity) {
                // If we had sent the message from the error handler we wouldn't have recorded the Activity Id and so we shouldn't expect to see it in the log.
                await this.sendMessageAndLogActivityId(context, `Activity ${ context.activity.replyToId } not found in the log.`);
            } else {
                await this.sendMessageAndLogActivityId(context, ` added '${ reactionsAdded[i].type }' regarding '${ activity.text }'`);
            }
        };
    }

    async onReactionsRemovedActivity(reactionsAdded, context) {
        for (var i = 0, len = reactionsAdded.length; i < len; i++) {
            // The ReplyToId property of the inbound MessageReaction Activity will correspond to a Message Activity that was previously sent from this bot.
            var activity = await this._log.find(context.activity.replyToId);
            if (!activity) {
                // If we had sent the message from the error handler we wouldn't have recorded the Activity Id and so we shouldn't expect to see it in the log.
                await this.sendMessageAndLogActivityId(context, `Activity ${ context.activity.replyToId } not found in the log.`);
            } else {
                await this.sendMessageAndLogActivityId(context, `You removed '${ reactionsAdded[i].type }' regarding '${ activity.text }'`);
            }
        };
    }

    async sendMessageAndLogActivityId(context, text) {
        const replyActivity = MessageFactory.text(text);
        const resourceResponse = await context.sendActivity(replyActivity);
        await this._log.append(resourceResponse.id, replyActivity);
    }

   /**
     * Send a welcome message along with suggested actions for the user to click.
     * @param {TurnContext} turnContext A TurnContext instance containing all the data needed for processing this conversation turn.
     */
    async sendWelcomeMessage(turnContext) {
        const { activity } = turnContext;

        // Iterate over all new members added to the conversation.
        for (const idx in activity.membersAdded) {
            if (activity.membersAdded[idx].id !== activity.recipient.id) {
                const welcomeMessage = `Welcome to suggestedActionsBot ${ activity.membersAdded[idx].name }. ` +
                    'This bot will introduce you to Suggested Actions. ' +
                    'Please select an option:';
                await turnContext.sendActivity(welcomeMessage);
                await this.sendSuggestedActions(turnContext);
            }
        }
    }

    /**
     * Send suggested actions to the user.
     * @param {TurnContext} turnContext A TurnContext instance containing all the data needed for processing this conversation turn.
     */
     async sendSuggestedActions(turnContext) {
        const card = CardFactory.heroCard(
            'Was möchten Sie machen?',
            [],
            actions
       );
       
       const message = MessageFactory.attachment(card);
       await turnContext.sendActivity(message);
       // await turnContext.sendActivity(reply);
    }
}



function GetJustInTimeCardAttachment() {
    return CardFactory.adaptiveCard({
        actions: [
            {
                type: 'Action.Submit',
                title: 'Continue',
                data: { msteams: { justInTimeInstall: true } }
            }
        ],
        body: [
            {
                text: 'Looks like you have not used Action Messaging Extension app in this team/chat. Please click **Continue** to add this app.',
                type: 'TextBlock',
                wrap: 'bolder'
            }
        ],
        type: 'AdaptiveCard',
        version: '1.0'
    });
}



module.exports.MessageReactionBot = MessageReactionBot;
