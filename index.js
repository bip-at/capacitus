// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

// index.js is used to setup and configure your bot

// Import required pckages
const path = require('path');

// Read botFilePath and botFileSecret from .env file.
const ENV_FILE = path.join(__dirname, '.env');
require('dotenv').config({ path: ENV_FILE });

const restify = require('restify');
const https = require('https');

const util = require("util");

// Import required bot services.
// See https://aka.ms/bot-services to learn more about the different parts of a bot.
const { BotFrameworkAdapter, MemoryStorage, ActivityTypes } = require('botbuilder');

const { ActivityLog } = require('./activityLog');
const { MessageReactionBot } = require('./bots/messageReactionBot');

// Create adapter.
// See https://aka.ms/about-bot-adapter to learn more about adapters.
const adapter = new BotFrameworkAdapter({
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword
});

adapter.onTurnError = async (context, error) => {
    // This check writes out errors to console log .vs. app insights.
    // NOTE: In production environment, you should consider logging this to Azure
    //       application insights. See https://aka.ms/bottelemetry for telemetry 
    //       configuration instructions.
    console.error(`\n [onTurnError] unhandled error: ${ error }`);

    // Send a trace activity, which will be displayed in Bot Framework Emulator
    await context.sendTraceActivity(
        'OnTurnError Trace',
        `${ error }`,
        'https://www.botframework.com/schemas/error',
        'TurnError'
    );

    // Send a message to the user
    await context.sendActivity('The bot encountered an error or bug.');
    await context.sendActivity('To continue to run this bot, please fix the bot source code.');
};

// Create storage and Activity Log used for tracking sent messages.
const memoryStorage = new MemoryStorage();
const activityLog = new ActivityLog(memoryStorage);

//const conversationReferences = {};
const garageShortNames = {};

// Array of {garageID}_{threshold} values which have been informed. 
const alreadyNotified = [];


// Create the bot that will handle incoming messages.
const bot = new MessageReactionBot(activityLog, garageShortNames);

const channels = ['19:7756442553ad46f78fdbf094a06e01fc@thread.tacv2'];
const garageShortName = ['KTG'];


// Create HTTP server.
const server = restify.createServer();

server.use(restify.plugins.bodyParser({
    requestBodyOnGet: true
}));

server.use(restify.plugins.queryParser());

server.listen(process.env.port || process.env.PORT || 3978, function() {
    console.log(`\n${ server.name } listening to ${ server.url }`);
});

// Listen for incoming requests.
server.post('/api/messages', (req, res) => {
    //console.log(`request: ${ JSON.stringify(req) }`);
    console.log(`request: ${ JSON.stringify(req.headers) }`);

    adapter.processActivity(req, res, async (context) => {
        await bot.run(context);
    });
});

server.get('/api/send', async (req, res) => {
    this.sendSMS(res);
});

//Vorwarnstand Garage OÖN voll (bei 85 freien Plätzen Gesamtzähler) per sms 
//senden an 1.) 0664/8740036 und 0664/2452337

exports.sendSMS = this.sendSMS;

this.sendSMS = async function(res) {
    console.log("calling func");

    var postData = JSON.stringify({
        'message' : 'Anzahl freier Parkplätze unter 85',
        'sender' : 'BIP',
        'recipients' : [
            {'msisdn' : '4369919037848'},
            {'msisdn' : '436648740036'}
        ]
    });

    const apiToken = "oBzFhX4ST1SpWB4oNUdgBGRmuNUcOgKSAXEabcSl-eFjDWuWGtGF5zNoXfP6uRIY";
    const encodedAuth = Buffer.from(`${apiToken}:`).toString("base64");
      
    var options = {
        hostname: 'gatewayapi.com',
        port: 443,
        path: '/rest/mtsms',
        method: 'POST',
        headers: {
             'Content-Type': 'application/json',
             Authorization: `Basic ${encodedAuth}`,
            }
      };
            
    
    var req = https.request(options, (resp) => {
        let data = '';
      
        // A chunk of data has been received.
        resp.on('data', (chunk) => {
          data += chunk;
        });
      
        // The whole response has been received. Print out the result.
        resp.on('end', () => {
            let responseData = JSON.parse(data);
            console.log(JSON.parse(data));
        //    res.setHeader('Content-Type', 'application/json');
            res.writeHead(200);
            res.write(JSON.stringify(responseData));
            res.end();
        });
      
    });

    req.on('error', (e) => {
        console.error(e);
    });
      
    req.write(postData);
    req.end();      
}

server.get('/api/notify', async (req, res) => {
    console.log('Got request '+req);

    var capacityValue = ''+req.query.value;
    if(capacityValue.length >3) {
        capacityValue = parseInt(capacityValue.slice(-2), 10);
    }
    console.log("cap value string; "+capacityValue);
    Object.values(garageShortNames).forEach(function (conversationReference, i) {
        console.log('%d: %s', i, conversationReference);
        console.log('carparknumber %s', req.query.oid);
        console.log('garageShortNames %s', JSON.stringify(garageShortNames));

        const garageAndThresholdKey = req.query.CarParkNumber + "_" + conversationReference.threshold;

        if (req.query.oid != null && garageShortNames[req.query.oid] != null 
            && req.query.oid == conversationReference.garageShortName) {
            
 
            if (capacityValue)
            if (conversationReference.threshold > capacityValue && !alreadyNotified.includes(garageAndThresholdKey)) {
                alreadyNotified.push(garageAndThresholdKey);

                if (req.query.oid == 64) {
                    sendSMS(res);
                }
            
                adapter.continueConversation(conversationReference, async turnContext => {
                    await turnContext.sendActivity("Carpark number: " + req.query.oid + " available: "+capacityValue + " threshold: "+conversationReference.threshold);
                });


            } else if (conversationReference.threshold <= capacityValue) {
                for( var i = 0; i < alreadyNotified.length; i++){ 
    
                    if ( alreadyNotified[i] === garageAndThresholdKey) { 
                
                        alreadyNotified.splice(i, 1); 
                    }
                
                }
            }
        }
    });



    res.setHeader('Content-Type', 'text/html');
    res.writeHead(200);
    res.write('<html><body><h1>Proactive messages have been sent.</h1></body></html>');
    res.end();
});
